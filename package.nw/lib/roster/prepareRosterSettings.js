function prepareRosterSettings()
{
  let rosterSettings = {
    bands: {},
    modes: {},
    callMode: CR.rosterSettings.callsign,
    onlyHits: false,
    isAwardTracker: false,
    now: timeNowSec()
  }

  if (rosterSettings.callMode == "hits")
  {
    rosterSettings.callMode = "all"
    rosterSettings.onlyHits = true;
  }
  if (referenceNeed.value == LOGBOOK_AWARD_TRACKER)
  {
    rosterSettings.callMode = "all";
    rosterSettings.onlyHits = false;
    rosterSettings.isAwardTracker = true;
    CR.rosterSettings.huntNeed = huntNeed.value = "confirmed";
  }
  // this appears to be determine if we should show the OAMS column
  // if the user is not in offline mode and has OAMS enabled, this could
  // be it's own function maybe?
  rosterSettings.canMsg =
    window.opener.GT.mapSettings.offlineMode == false &&
    window.opener.GT.appSettings.gtShareEnable == true &&
    window.opener.GT.appSettings.gtMsgEnable == true;

  // The following 3 sections deal with QSLing, do we break them out
  // individually or lump them into a qslUser function that sets
  // all three at the same time?
  // this section is for LoTW users, can be a function
  if (window.opener.GT.callsignLookups.lotwUseEnable == true)
  {
    usesLoTWDiv.style.display = "";
    if (CR.rosterSettings.usesLoTW == true)
    {
      maxLoTW.style.display = "";
      maxLoTWView.style.display = "";
    }
    else
    {
      maxLoTW.style.display = "none";
      maxLoTWView.style.display = "none";
    }
  }
  else
  {
    usesLoTWDiv.style.display = "none";
    maxLoTW.style.display = "none";
    maxLoTWView.style.display = "none";
  }

  if (CR.rosterSettings.huntNeed == "mixed")
  {
    rosterSettings.huntIndex = CR.confirmed;
    rosterSettings.workedIndex = CR.worked;
    rosterSettings.layeredMode = LAYERED_MODE_FOR[String(CR.rosterSettings.reference)];
  }
  else if (CR.rosterSettings.huntNeed == "worked")
  {
    rosterSettings.huntIndex = CR.worked;
    rosterSettings.workedIndex = false;
    rosterSettings.layeredMode = false;
  }
  else if (CR.rosterSettings.huntNeed == "confirmed")
  {
    rosterSettings.huntIndex = CR.confirmed;
    rosterSettings.workedIndex = CR.worked;
    rosterSettings.layeredMode = false;
  }
  else
  {
    console.log("Invalid/Unknown huntNeed");
    rosterSettings.huntIndex = false;
    rosterSettings.workedIndex = false;
    rosterSettings.layeredMode = false;
  }

  return rosterSettings;
}
